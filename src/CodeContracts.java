import java.util.Date;

//https://github.com/C4J-Team/C4J-Eclipse-Plugin
public class CodeContracts {

	public static void main(String[] args) {
		System.out.println("Program started");
		System.out.println("Clock for Code Contract Test:"
				+ new Date().toLocaleString());

		testTime(5);
		System.out.println("Program ended");
	}

	public static void testTime(int hour) {
		assert hour < 25 : "hour can not be bigger than 24";
		assert hour > -1 : "hour has to be an integer value";

		System.out.println("Accenture Quality time:" + hour);
	}
}
